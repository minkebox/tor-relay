#! /bin/sh

trap "killall tor sleep; exit" TERM INT

# Permissions
chmod 700 /var/lib/tor /etc/tor
chmod 600 /etc/tor/torrc

# Override DNS server - we want to make sure we use an external server
echo "nameserver 1.1.1.1" > /etc/resolv.conf
/usr/bin/tor &

sleep 2147483647d &
wait "$!"
