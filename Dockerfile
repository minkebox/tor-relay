FROM alpine:latest

RUN apk --no-cache add tor

COPY root/ /

ENTRYPOINT ["/startup.sh"]
